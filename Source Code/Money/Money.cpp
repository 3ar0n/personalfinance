﻿#include "stdafx.h"
#include "Money.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MONEY, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MONEY));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MONEY));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MONEY);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 600, 540, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	INITCOMMONCONTROLSEX icex;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_NOTIFY, OnNotify);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_CLOSE, OnClose);
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void OnClose(HWND hWnd)
{
	fileSave(g_head, L"Save.data");
	record_Destroy(g_head);
	DestroyWindow(hWnd);
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	int textLen1, textLen2;
	TCHAR blank[10] = L"";
	TCHAR* buf_detail = NULL;
	TCHAR* buf_money = NULL;
	switch (id)
	{
	case IDC_CATEGORY:
		if (codeNotify == CBN_SELCHANGE) {
			g_categoryIndex = ComboBox_GetCurSel(hwndCtl);
		}
		break;
	case IDC_ADD:
		textLen1 = GetWindowTextLength(g_hwndList[CH_EDIT_DETAIL]) + 1;
		textLen2 = GetWindowTextLength(g_hwndList[CH_EDIT_MONEY]) + 1;
		
		if (g_categoryIndex < 0 || textLen1 < 2 || textLen2 < 2)
			if (g_categoryIndex < 0)
				MessageBox(hWnd, L"Chưa chọn loại chi tiêu", L"Lỗi", MB_OK);
			else
				MessageBox(hWnd, L"Không đủ dữ liệu", L"Lỗi", MB_OK);
		else if (textLen2 > 18)
			MessageBox(hWnd, L"Số tiền vượt quá giới hạn", L"Lỗi", MB_OK);
		else
		{
			buf_detail = new WCHAR[textLen1];
			buf_money = new WCHAR[textLen2];
			GetWindowText(g_hwndList[CH_EDIT_DETAIL], buf_detail, textLen1);
			GetWindowText(g_hwndList[CH_EDIT_MONEY], buf_money, textLen2);
			if (text_checkInput(buf_money, textLen2, 0) == FALSE)
				MessageBox(hWnd, L"Nhập sai số tiền", L"Lỗi", MB_OK);
			else
			{
				SetWindowText(g_hwndList[CH_EDIT_DETAIL], blank);
				SetWindowText(g_hwndList[CH_EDIT_MONEY], blank);

				record_AddStack(g_head, g_categoryIndex, buf_detail, _wtoi64(buf_money));
				record_Refresh(g_head, g_hwndList[CH_LISTVIEW], g_hwndList[CH_TEXT_SUM]);
				
				g_hasData = TRUE;
				InvalidateRect(hWnd, NULL, TRUE);
			}
			delete[] buf_detail;
			delete[] buf_money;
		}
		break;
	case IDC_DEL:
		Button_Enable(g_hwndList[CH_BTN_DEL], FALSE);
		record_Delete(g_head, preDelItem);
		preDelItem = NULL;
		if (g_head == NULL)
			g_hasData = FALSE;
		record_Refresh(g_head, g_hwndList[CH_LISTVIEW], g_hwndList[CH_TEXT_SUM]);
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		fileSave(g_head, L"Save.data");
		record_Destroy(g_head);
		DestroyWindow(hWnd);
		break;
	}
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	HWND newWnd = NULL;

	newWnd = CreateWindow(L"BUTTON", L" Thêm mục chi tiêu ", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, 10, 10, 170, 250, hWnd, NULL, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd, 4);	// index = 0

	newWnd = CreateWindow(L"BUTTON", L" Danh sách ", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, 290, 10, 320, 370, hWnd, NULL, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd, 4);	// index = 1

	newWnd = CreateWindow(L"BUTTON", L" Thống kê ", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, 10, 270, 170, 70, hWnd, NULL, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd, 4);	// index = 2

	newWnd = CreateWindow(L"STATIC", L"Danh mục", WS_CHILD | WS_VISIBLE | SS_LEFT, 20, 40, 150, 20, hWnd, NULL, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd);		// index = 3

	newWnd = CreateWindow(L"COMBOBOX", L"", WS_CHILD | WS_VISIBLE | CBS_DROPDOWN, 20, 60, 150, 24, hWnd, (HMENU)IDC_CATEGORY, hInst, NULL);
	for (int i = 0; i < 7; i++)
		ComboBox_AddString(newWnd, category[i]);
	vector_Add(g_hwndList, g_hwndIndex, newWnd);		// index = 4

	newWnd = CreateWindow(L"STATIC", L"Chi tiết", WS_CHILD | WS_VISIBLE | SS_LEFT, 20, 100, 150, 20, hWnd, NULL, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd);		// index = 5

	newWnd = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL, 20, 120, 150, 24, hWnd, NULL, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd);		// index = 6

	newWnd = CreateWindow(L"STATIC", L"Số tiền", WS_CHILD | WS_VISIBLE | SS_LEFT, 20, 160, 150, 20, hWnd, NULL, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd);		// index = 7

	newWnd = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL, 20, 180, 150, 24, hWnd, NULL, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd);		// index = 8

	newWnd = CreateWindow(L"BUTTON", L"Thêm", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 55, 220, 80, 25, hWnd, (HMENU)IDC_ADD, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd); 		// index = 9

	newWnd = CreateWindow(WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | LVS_NOSORTHEADER | LVS_SINGLESEL | WS_BORDER,
		300, 40, 300, 290, hWnd, (HMENU)IDC_LIST, hInst, NULL);
	ListView_SetExtendedListViewStyleEx(newWnd, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);
	LVCOLUMN lvCol;
	lvCol.mask = LVCF_TEXT | LVCF_WIDTH;
	lvCol.pszText = L"Loại chi tiêu";
	lvCol.cx = 100;
	ListView_InsertColumn(newWnd, 0, &lvCol);

	lvCol.mask = LVCF_TEXT | LVCF_WIDTH;
	lvCol.pszText = L"Chi tiết";
	lvCol.cx = 100;
	ListView_InsertColumn(newWnd, 1, &lvCol);

	lvCol.mask = LVCF_TEXT | LVCF_WIDTH | LVCF_FMT;
	lvCol.fmt = LVCFMT_RIGHT;
	lvCol.pszText = L"Số tiền";
	lvCol.cx = 100;
	ListView_InsertColumn(newWnd, 2, &lvCol);
	vector_Add(g_hwndList, g_hwndIndex, newWnd);		// index = 10

	newWnd = CreateWindow(L"BUTTON", L"Xóa", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 500, 340, 100, 25, hWnd, (HMENU)IDC_DEL, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd); 		// index = 11
	Button_Enable(newWnd, FALSE);

	newWnd = CreateWindow(L"STATIC", L"Tổng", WS_CHILD | WS_VISIBLE | SS_RIGHT, 300, 340, 50, 25, hWnd, NULL, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd);		// index = 12

	newWnd = CreateWindow(L"STATIC", L"", WS_CHILD | WS_VISIBLE | SS_RIGHT, 300, 340, 100, 25, hWnd, NULL, hInst, NULL);
	vector_Add(g_hwndList, g_hwndIndex, newWnd);		// index = 13

	fileLoad(g_head, L"Save.data");
	record_Refresh(g_head, g_hwndList[CH_LISTVIEW], g_hwndList[CH_TEXT_SUM]);
	if (g_head != NULL)
		g_hasData = TRUE;
	InvalidateRect(hWnd, NULL, TRUE);

	return TRUE;
}

void OnDestroy(HWND hWnd)
{
	PostQuitMessage(0);
}

LRESULT OnNotify(HWND hWnd, int idFrom, NMHDR *pnm)
{
	HWND hCurrent = pnm->hwndFrom;

	switch (pnm->code)
	{
	case NM_CLICK:
		if (hCurrent == g_hwndList[CH_LISTVIEW])
		{
			int i = ListView_GetNextItem(g_hwndList[CH_LISTVIEW], -1, LVNI_SELECTED);
			LVITEM lvitem;
			if (i != -1) {
				lvitem.mask = LVIF_PARAM;
				lvitem.iItem = i;
				lvitem.iSubItem = 0;
				ListView_GetItem(g_hwndList[CH_LISTVIEW], &lvitem);

				preDelItem = (node)lvitem.lParam;
				Button_Enable(g_hwndList[CH_BTN_DEL], TRUE);
			}
			else {
				preDelItem = NULL;
				Button_Enable(g_hwndList[CH_BTN_DEL], FALSE);
			}
		}
		break;
	}
	return 0;
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);

	int nX = 10 + S.w0 / 2;
	int nY = 25 + S.h0 + S.h2 / 2;
	DWORD dwRadius;
	if (S.w0 > (S.h2 - 10))
		dwRadius = S.h2 / 2 - 15;
	else
		dwRadius = S.w0 / 2 - 10;

	if (g_hasData == TRUE)
	{
		float xSweepAngle = 0, xStartAngle = 90;
		float* ratio = record_getRatio(g_head);
		for (int i = 0; i < 7; i++)
		{
			hdc = GetDC(hWnd);
			BeginPath(hdc);
			SelectObject(hdc, CreatePen(PS_NULL, 1, categoryColor[i]));
			SelectObject(hdc, GetStockObject(DC_BRUSH));
			SetDCBrushColor(hdc, categoryColor[i]);
			MoveToEx(hdc, nX, nY, (LPPOINT)NULL);
			xSweepAngle = ratio[i] * (-360);
			AngleArc(hdc, nX, nY, dwRadius, xStartAngle, xSweepAngle);
			LineTo(hdc, nX, nY);
			xStartAngle += xSweepAngle;
			EndPath(hdc);
			StrokeAndFillPath(hdc);
			ReleaseDC(hWnd, hdc);
		}
		delete[] ratio;
	}
	EndPaint(hWnd, &ps);
}

void OnSize(HWND hWnd, UINT state, int cx, int cy)
{
	if (cy < 384)
		cy = 384;
	if (cx < 510)
		cx = 510;
	S.mWidth = cx;
	S.mHeight = cy;
	S.w0 = (cx - 30) * 1 / 3;
	S.w1 = (cx - 30) * 2 / 3;
	S.h0 = (cy - 30) * 3 / 5;
	S.h1 = cy - 20;
	S.h2 = (cy - 30) * 2 / 5;
	int dy = (S.h0 - 163) / 5;

	MoveWindow(g_hwndList[0], 10, 10, S.w0, S.h0, TRUE);
	MoveWindow(g_hwndList[1], 20 + S.w0, 10, S.w1, S.h1, TRUE);
	MoveWindow(g_hwndList[2], 10, 20 + S.h0, S.w0, S.h2, TRUE);
	MoveWindow(g_hwndList[3], 20, 15 + dy * 1, S.w0 - 20, 20, TRUE);
	MoveWindow(g_hwndList[4], 20, 35 + dy * 1, S.w0 - 20, 24, TRUE);
	MoveWindow(g_hwndList[5], 20, 59 + dy * 2, S.w0 - 20, 20, TRUE);
	MoveWindow(g_hwndList[6], 20, 79 + dy * 2, S.w0 - 20, 24, TRUE);
	MoveWindow(g_hwndList[7], 20, 103 + dy * 3, S.w0 - 20, 20, TRUE);
	MoveWindow(g_hwndList[8], 20, 123 + dy * 3, S.w0 - 20, 24, TRUE);
	MoveWindow(g_hwndList[9], 20, 147 + dy * 4, 80, 25, TRUE);
	MoveWindow(g_hwndList[CH_LISTVIEW], 30 + S.w0, 40, S.w1 - 20, S.h1 - 75, TRUE);
	ListView_SetColumnWidth(g_hwndList[CH_LISTVIEW], 0, (S.w1 - 20) * 3 / 10);
	ListView_SetColumnWidth(g_hwndList[CH_LISTVIEW], 1, (S.w1 - 20) * 4 / 10);
	ListView_SetColumnWidth(g_hwndList[CH_LISTVIEW], 2, (S.w1 - 20) * 3 / 10);
	MoveWindow(g_hwndList[11], 30 + S.w0, S.h1 - 25, 80, 25, TRUE);
	MoveWindow(g_hwndList[12], S.w0 + S.w1 - 210, S.h1 - 20, 50, 25, TRUE);
	MoveWindow(g_hwndList[13], S.w0 + S.w1 - 150, S.h1 - 20, 150, 25, TRUE);
	InvalidateRect(hWnd, NULL, TRUE);
}

void vector_Add(vector<HWND> &hwndList, int &hwndIndex, HWND hwndNew, int fontSize)
{
	SendMessage(hwndNew, WM_SETFONT, WPARAM(setFont(fontSize)), TRUE);
	hwndList.push_back(hwndNew);
	hwndIndex++;
}

bool text_checkInput(LPCWSTR string, int len, int type)
{
	bool checkFlag = TRUE;
	int j;
	if (type == 0)		// number
	{
		WCHAR sample[10] = { L'0', L'1', L'2', L'3', L'4', L'5', L'6', L'7', L'8', L'9' };
		for (int i = 0; i < len - 1; i++)
		{
			checkFlag = FALSE;
			for (j = 0; j < 10; j++)
			{
				if (string[i] == sample[j])
				{
					checkFlag = TRUE;
					break;
				}
			}
			if (j == 10)
				return FALSE;
		}
	}
	return checkFlag;
}

void record_AddQueue(node &head, short CategoryIndex, LPCWSTR Detail, __int64 Money)
{
	node p = new record;
	
	p->categoryIndex = CategoryIndex;
	StrCpy(p->detail, Detail);
	p->money = Money;
	p->next = NULL;

	if (head == NULL)
	{
		head = p;
		p->prev = NULL;
	}
	else
	{
		node q = head;
		while (q->next != NULL)
			q = q->next;
		q->next = p;
		p->prev = q;
	}
}

void record_AddStack(node &head, short CategoryIndex, LPCWSTR Detail, __int64 Money)
{
	node p = new record;
	p->categoryIndex = CategoryIndex;
	StrCpy(p->detail, Detail);
	p->money = Money;
	p->prev = NULL;

	if (head == NULL) {
		head = p;
		p->next = NULL;
	}
	else {
		head->prev = p;
		p->next = head;
		head = p;
	}
}

void record_Delete(node &head, node p)
{
	if (p != NULL && head != NULL)
	{
		node o = p->prev;
		node q = p->next;
		if (o != NULL)
			o->next = q;
		else	// p = head
			head = q;

		if (q != NULL)
			q->prev = o;
		delete p;
	}
}

void record_Destroy(node head)
{
	if (head != NULL)
	{
		node p, q;
		p = head;
		while (p)
		{
			q = p->next;
			delete p;
			p = q;
		}
	}
}

void record_LoadList(node head, HWND hListView)
{
	TCHAR* buffer = new TCHAR[MAX_LOADSTRING];
	int i = 0;
	node p = head;
	LV_ITEM lv;
	while (p)
	{
		_i64tow_s(p->money, buffer, MAX_LOADSTRING, 10);

		lv.mask = LVIF_TEXT | LVIF_PARAM;
		lv.iItem = i;
		lv.iSubItem = 0;
		lv.pszText = category[p->categoryIndex];
		lv.lParam = (LPARAM)p;
		ListView_InsertItem(hListView, &lv);
		ListView_SetItemText(hListView, i, 1, p->detail);
		ListView_SetItemText(hListView, i, 2, buffer);
		
		p = p->next;
		i++;
	}
	delete[] buffer;
}

void record_Refresh(node head, HWND hListView, HWND hSummary)
{
	ListView_DeleteAllItems(hListView);
	record_LoadList(head, hListView);

	// Summary
	TCHAR summary[MAX_LOADSTRING];
	__int64 Sum = record_getSum(head);
	_i64tow_s(Sum, summary, MAX_LOADSTRING, 10);
	SetWindowText(hSummary, summary);
}

__int64 record_getSum(node head)
{
	__int64 Sum = 0;
	if (head)
	{
		node p = head;
		while (p)
		{
			Sum += p->money;
			p = p->next;
		}
	}
	return Sum;
}

float* record_getRatio(node head)
{
	float* Ratio = new float[7];
	for (int i = 0; i < 7; i++)
		Ratio[i] = 0;

	if (head)
	{
		_int64 Sum = record_getSum(head);
		node p = head;
		while (p)
		{
			Ratio[p->categoryIndex] += (float)p->money / Sum;
			p = p->next;
		}
	}
	return Ratio;
}

unsigned short* convert_int64toArray(__int64 x)
{
	unsigned short* s = new unsigned short[4];
	for (int i = 3; i >= 0; i--)
	{
		s[i] = x % 65536;
		x = x / 65536;
	}
	return s;
}

__int64 convert_arraytoInt64(unsigned short* s)
{
	__int64 x = 0;
	for (int i = 0; i < 4; i++)
	{
		x = x * 65536;
		x = x + s[i];
	}
	return x;
}

LPCWSTR convert_recordToString(node p)
{
	TCHAR* string = new TCHAR[MAX_LOADSTRING];
	int len, i;

	StrCpy(string, _T(" "));
	string[0] = 0x0000 + (__int64)p->categoryIndex + 1;
	
	i = 1;
	len = wcslen(p->detail);
	StrCat(string, _T(" "));
	string[i] = 0x0000 + len;
	i = i + len + 1;
	StrCat(string, p->detail);

	unsigned short* s = convert_int64toArray(p->money);
	StrCat(string, _T("    "));
	for (int j = 0; j < 4; j++) {
		string[i + j] = 0xFFFF - s[j];
	}
	StrCat(string, _T("\n"));

	return string;
}

void fileSave(node head, LPCWSTR fileName)
{
	HANDLE File;
	WCHAR* buffer = new TCHAR[MAX_LOADSTRING];
	DWORD dwBytesWritten;
	File = CreateFile(fileName, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (File != INVALID_HANDLE_VALUE)
	{
		node p = head;
		while (p)
		{
			StrCpy(buffer, convert_recordToString(p));
			WriteFile(File, buffer, wcslen(buffer) * sizeof(WCHAR), &dwBytesWritten, NULL);
			p = p->next;
		}
		CloseHandle(File);
		delete[] buffer;
	}
}

void fileLoad(node &head, LPCWSTR fileName)
{
	HANDLE File;
	unsigned short* num = new unsigned short;
	unsigned short* Array = new unsigned short[4];
	__int64 i_money;
	short i_category;
	TCHAR* buf_detail = NULL;
	DWORD dwBytesRead;

	File = CreateFile(fileName, GENERIC_READ, FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (File != INVALID_HANDLE_VALUE)
	{
		int TotalSize = GetFileSize(File, NULL);
		int i = 0;
		while (i < TotalSize)
		{
			/* Cấu trúc 1 dòng trong file .data
				[2 bytes] trường "CategoryIndex" (giá trị +1)
				[2 bytes] n: số kí tự trường "Detail"
				[n*2 bytes] trường "Detail"
				[8 bytes] trường "Money" (giá trị đảo bit)
				[2 byte] kí tự xuống dòng */			
			ReadFile(File, num, 2, &dwBytesRead, NULL);
			i_category = *num - 1;
			i = i + 2;

			ReadFile(File, num, 2, &dwBytesRead, NULL);
			buf_detail = new WCHAR[*num + 1];
			ReadFile(File, buf_detail, *num * 2, &dwBytesRead, NULL);
			buf_detail[*num] = L'\0';
			i = i + 2 + (*num) * 2;

			for (int k = 0; k < 4; k++) {
				ReadFile(File, Array + k, 2, &dwBytesRead, NULL);
				Array[k] = 0xFFFF - Array[k];
			}	
			i_money = convert_arraytoInt64(Array);
			i = i + 8;

			ReadFile(File, num, 2, &dwBytesRead, NULL);
			i = i + 2;

			record_AddQueue(head, i_category, buf_detail, i_money);
			delete[] buf_detail;
		}
		CloseHandle(File);
	}
	delete num;
	delete[] Array;
}