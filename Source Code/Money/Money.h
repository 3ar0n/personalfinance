﻿#pragma once

#include "stdafx.h"
#include "resource.h"
#include "windowsx.h"
#include <vector>
#include <string>

//using StrCpy, StrNCat
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

// listview & treeview
#include <commctrl.h>
#pragma comment(lib, "Comctl32.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

using namespace std;
#define MAX_LOADSTRING 100
#define CH_GROUP_1 0
#define CH_GROUP_2 1
#define CH_GROUP_3 2
#define CH_EDIT_DETAIL 6
#define CH_EDIT_MONEY 8
#define CH_BTN_ADD 9
#define CH_LISTVIEW 10
#define CH_BTN_DEL 11
#define CH_TEXT_SUM 13

// Global Variables
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

typedef struct record * node;
struct record
{
	short categoryIndex;
	TCHAR detail[MAX_LOADSTRING];
	__int64 money;
	node prev;
	node next;
};
node g_head = NULL, preDelItem = NULL;

struct hwndSize
{
	int mWidth, mHeight, w0, w1, h0, h1, h2;
};
hwndSize S;
bool g_hasData = FALSE;
vector <HWND> g_hwndList;
int g_hwndIndex = -1;
short g_categoryIndex = -1;
TCHAR* category[7] = { L"Ăn uống", L"Di chuyển", L"Nhà cửa", L"Xe cộ", L"Nhu yếu phẩm", L"Dịch vụ", L"Khác" };
COLORREF categoryColor[7] = { RGB(255, 92, 92), RGB(255, 174, 51), RGB(255, 255, 92),  RGB(0, 192, 64),  RGB(46, 230, 230),  RGB(77, 166, 255),  RGB(174, 133, 255) };

// Funtions
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);
void OnSize(HWND hWnd, UINT state, int cx, int cy);
void OnClose(HWND hWnd);
LRESULT OnNotify(HWND hWnd, int idFrom, NMHDR *pnm);

void vector_Add(vector<HWND> &hwndList, int &hwndIndex, HWND hwndNew, int fontSize = 2);
bool text_checkInput(LPCWSTR string, int len, int type);

void record_AddQueue(node &head, short CategoryIndex, LPCWSTR Detail, __int64 Money);
void record_AddStack(node &head, short CategoryIndex, LPCWSTR Detail, __int64 Money);
void record_Delete(node &head, node p);
void record_Destroy(node head);
void record_LoadList(node head, HWND hListView);
__int64 record_getSum(node head);
float* record_getRatio(node head);

void record_Refresh(node head, HWND hListView, HWND hSummary);
LPCWSTR convert_recordToString(node p);
unsigned short* convert_int64toArray(__int64 x);
__int64 convert_arraytoInt64(unsigned short* s);
void fileSave(node head, LPCWSTR fileName);
void fileLoad(node &head, LPCWSTR fileName);


HFONT setFont(int dSize)
{
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(lf.lfHeight - dSize, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);
	return hFont;
}