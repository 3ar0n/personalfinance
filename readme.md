## Thông tin cá nhân
Họ và tên: **Đặng Quốc Thái**
MSSV: **1512501**

## Các chức năng
* Thêm vào danh sách 1 mục chi mới gồm có 3 thành phần: Danh mục, Chi tiết, Số tiền. Danh mục chi tiêu bao gồm: Ăn uống, Di chuyển, Nhà cửa, Xe cộ, Nhu yếu phẩm, Dịch vụ, Khác.
* Xóa mục chi tiêu trong danh sách hiển thị (ListView).
* Hiển thị danh sách trong ListView.
* Biểu đồ tròn hiển thị tỉ lệ chi tiêu.
* Danh sách, biểu đồ tự động cập nhật khi thêm/xóa mục chi tiêu
* Tự động lưu (khi thoát chương trình) và nhập (khi mở chương trình) danh sách vào tập tin Save.data (dữ liệu hỗ sợ tiếng Việt đã mã hóa) trong cùng thư mục.
* Tự động điều chỉnh giao diện người dùng khi thay đổi kích thước cửa sổ chính.

## Hướng dẫn sử dụng
1. Chọn "Danh mục", nhập dữ liệu "Chi tiết", "Số tiền" rồi nhấn "Thêm" để thêm 1 mục chi tiêu mới
2. Nhấn chọn 1 mục trong danh sách hiển thị rồi nhấn "Xóa" để xóa 1 mục chi tiêu.

## Ghi chú
1. Thông thi lưu trong Save.data đã được mã hóa. Nếu tự ý sửa file bằng chương trình khác (Notepad++...) có thể dẫn đến lỗi, không chạy được chương trình

## Link Repository
<https://bitbucket.org/3ar0n/personalfinance.git>

## Link Youtube
<https://youtu.be/AInA4i61EsQ>